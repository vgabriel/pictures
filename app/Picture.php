<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model {

    protected $fillable = [
        'name',
        'path'
    ];
	public function album()
    {
        return $this->belongsTo('App\Album');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}