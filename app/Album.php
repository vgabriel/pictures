<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model {

    protected $fillable = [
        'album_name'
    ];

    public function pictures()
    {
        return $this->hasMany('App\Picture', 'album_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
