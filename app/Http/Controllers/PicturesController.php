<?php namespace App\Http\Controllers;

use App\Album;
use App\Picture;
use App\Http\Requests;
use Illuminate\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests\PreparePictureRequest;

class PicturesController extends Controller {

    public function __construct()
    {
        // Allow only logged in users
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Guard $auth
     * @return Response
     */
	public function index(Guard $auth)
	{
        $albums = $auth->user()->albums()->lists('album_name', 'id');

        return view('pictures.upload', compact('albums'));
	}

    /**
     * View pictures for a particular user
     *
     * @param Guard $auth
     * @return \Illuminate\View\View
     */
    public function view(Guard $auth)
    {
        $picture = $auth->user()->pictures()->get();

        return view('pictures.view', compact('picture'));
    }

    /**
     * Show the form for creating a new resource.
     * @param PreparePictureRequest $request
     * @param Image $img
     * @return Response
     */
	public function upload(PreparePictureRequest $request, Image $img, Guard $auth)
	{
            $destination_path = public_path() . '/images/' . $auth->user()->id . '/';
            File::makeDirectory($destination_path, $mode = 0777, true, true);
            $image = $request->file('file');

            // Save images to disk
            $filename = $image->getClientOriginalName();
            $upload = $img->make($image)->fit(278);
            $upload->save($destination_path . '/' . $filename);
            $db_path = 'images/' . $auth->user()->id . '/' . $filename;

            $pic =
                [
                    'name' => $filename,
                    'path' => $db_path,
                    'album_id' => $request['albums'],
                    'user_id' => $auth->user()->id,
                ];
            // Save images to database
            $pictures = new Picture();
            $pictures->name = $pic['name'];
            $pictures->path = $pic['path'];
            $pictures->album_id = $pic['album_id'];
            $pictures->user_id = $pic['user_id'];
            $pictures->save();

            return redirect('pictures/view');
	}

}
