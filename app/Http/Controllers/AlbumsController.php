<?php namespace App\Http\Controllers;

use App\Album;
use App\Http\Requests;
use App\Http\Requests\PrepareAlbumRequest;
use Illuminate\Auth\Guard;

class AlbumsController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return view('albums.create');
    }

    /**
     * View albums for a particular user
     *
     * @param Guard $auth
     * @return \Illuminate\View\View
     * @internal param Album $album
     */
    public function viewAlbums(Guard $auth)
    {
        $albums = $auth->user()->albums()->get();

        return view('albums.view', compact('albums'));
    }

    /**
     * View pictures for a particular album
     *
     * @param $id
     * @param Album $album
     * @return \Illuminate\View\View
     */
    public function viewAlbum($id, Album $album)
    {
        $album = $album->find($id);
        $picture = $album->pictures()->lists('name', 'path');

        return view('albums.album', compact('picture'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PrepareAlbumRequest $request
     * @param Guard $auth
     * @return Response
     */
	public function store(PrepareAlbumRequest $request, Guard $auth)
	{
        $album = ['album_name' => $request->input('album_name')];

        $auth->user()->albums()->create($album);

        return redirect('pictures/upload');
	}

}
