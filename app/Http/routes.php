<?php

/*
 * The home page
*/
Route::get('/', 'PagesController@home');

/*
 * Authentication
*/
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

/*
 * Albums
 */
Route::get('albums/create', 'AlbumsController@index');
Route::post('albums/create', 'AlbumsController@store');

Route::get('albums/view', 'AlbumsController@viewAlbums');
Route::get('albums/{name}', array('uses' => 'AlbumsController@viewAlbum', 'as' => 'album'));

/*
 * Pictures
 */
Route::get('pictures/upload', 'PicturesController@index');
Route::post('pictures/upload', 'PicturesController@upload');

Route::get('pictures/view', 'PicturesController@view');
