@extends('app')

@section('content')
    <!-- Album Name Form Input -->
    {!! Form::open() !!}
        <div class="form-group">
            {!! Form::label('album_name', 'Album Name:') !!}
            {!! Form::text('album_name', null, ['class' => 'form-control']) !!}
        </div>
    <!--  Create Form Input -->
    <div class="form-group">
        {!! Form::submit('Create', ['class' => 'btn btn-primary form-control']) !!}
    </div>
    {!! Form::close() !!}
    @include('errors.list')

@endsection