@extends('app')

@section('content')
    <h1 class="page-heading">Your Albums</h1>
    @foreach($albums as $album)
        {!! HTML::linkRoute('album', $album->album_name, array($album->id), array('class' => 'btn btn-primary btn-lg')) !!}
    @endforeach
@endsection