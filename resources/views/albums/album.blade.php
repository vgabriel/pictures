@extends('app')

@section('content')
    @if($picture)
        <h1 class="page-heading">Your Album</h1>
        @foreach($picture as $key => $value)
            {!! Form::image($key, $value, array('class' => 'img-rounded')) !!}
        @endforeach
    @else
        <h1 class="page-heading">Your Album is empty</h1>
    @endif
@endsection