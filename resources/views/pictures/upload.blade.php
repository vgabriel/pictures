@extends('app')

@section('content')
    <h1 class="page-heading">Picture uploads</h1>
    {!! Form::open(['method' => 'POST', 'files' => true, 'action' => 'PicturesController@upload']) !!}
    <div class="form-group">
        {!! Form::label('files', 'Upload File', array('id' => 'upload_file')) !!}
        {!! Form::file('file', array('class'=>'btn btn-default btn-file' )) !!}
        </span>
    </div>

    <div class="form-group">
        {!! Form::label('album_name', 'Album Name', array('id' => 'album_name')) !!}
        {!! Form::select('albums', $albums, null, array('class'=>'form-control' )) !!}
    </div>
    <!--  Upload Picture Form Input -->
    <div class="form-group">
        {!! Form::submit('Upload Picture', ['id' => 'myDrop', 'class' => 'btn btn-primary form-control']) !!}
    </div>
    {!! Form::close() !!}
    @include('errors.list')

@endsection