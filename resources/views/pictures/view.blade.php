@extends('app')

@section('content')
        <h1 class="page-heading">View your pictures</h1>
    @foreach($picture as $pictures)
        {!! Form::image($pictures->path, $pictures->name, array('class' => 'img-rounded')) !!}
    @endforeach

@endsection